changes in the script to provide interactive exports to various formats
Index: chemical-structures-2.2.dfsg.0/tools/cmlwriter.py
===================================================================
--- chemical-structures-2.2.dfsg.0.orig/tools/cmlwriter.py
+++ chemical-structures-2.2.dfsg.0/tools/cmlwriter.py
@@ -17,6 +17,86 @@ class CMLWriter:
         self.fout = fout
         self.cml = cml_handler
         self.l10n = l10n_handler
+        self.scriptFormat="""
+    <script type='text/javascript'>
+      function getOtherFormat(obj){
+        i=obj.selectedIndex
+        if(i>0){
+         l=document.location+''
+         ext=obj.options[i].value
+         mol=l.replace(/.*chemical-structures\/(.*)_[a-z]*\.html$/,'$1')
+         newl='../convert.cgi?mol='+escape(mol)+"&ext="+ext
+         document.location=newl
+        }
+      }
+    </script>
+"""
+        self.selection="""
+              <select id="format" name="format" onChange="getOtherFormat(this)">
+                <option value="none">----------------</option>
+                <option value="alc">ALC -- Alchemy format</option>
+                <option value="bgf">BGF -- MSI BGF format</option>
+                <option value="box">BOX -- Dock 3.5 Box format</option>
+                <option value="bs">BS -- Ball and Stick format</option>
+                <option value="c3d1">C3D1 -- Chem3D Cartesian 1 format</option>
+                <option value="c3d2">C3D2 -- Chem3D Cartesian 2 format</option>
+                <option value="caccrt">CACCRT -- Cacao Cartesian format</option>
+                <option value="cache">CACHE -- CAChe MolStruct format [Write-only]</option>
+                <option value="cacint">CACINT -- Cacao Internal format [Write-only]</option>
+                <option value="cht">CHT -- Chemtool format [Write-only]</option>
+                <option value="cml">CML --  Chemical Markup Language</option>
+                <option value="cmlr">CMLR --  CML Reaction format</option>
+                <option value="com">COM -- Gaussian 98/03 Cartesian Input [Write-only]</option>
+                <option value="copy">COPY -- Copies raw text [Write-only]</option>
+                <option value="crk2d">CRK2D -- Chemical Resource Kit diagram format (2D)</option>
+                <option value="crk3d">CRK3D -- Chemical Resource Kit 3D format</option>
+                <option value="csr">CSR -- Accelrys/MSI Quanta CSR format [Write-only]</option>
+                <option value="cssr">CSSR -- CSD CSSR format [Write-only]</option>
+                <option value="ct">CT -- ChemDraw Connection Table format </option>
+                <option value="dmol">DMOL -- DMol3 coordinates format</option>
+                <option value="ent">ENT -- Protein Data Bank format</option>
+                <option value="feat">FEAT -- Feature format</option>
+                <option value="fh">FH -- Fenske-Hall Z-Matrix format [Write-only]</option>
+                <option value="fix">FIX -- SMILES FIX format [Write-only]</option>
+                <option value="fpt">FPT -- Fingerprint format [Write-only]</option>
+                <option value="fract">FRACT -- Free Form Fractional format</option>
+                <option value="fs">FS -- FastSearching</option>
+                <option value="gamin">GAMIN -- GAMESS Input [Write-only]</option>
+                <option value="gau">GAU -- Gaussian 98/03 Cartesian Input [Write-only]</option>
+                <option value="gpr">GPR -- Ghemical format</option>
+                <option value="gr96">GR96 -- GROMOS96 format [Write-only]</option>
+                <option value="hin">HIN -- HyperChem HIN format</option>
+                <option value="inp">INP -- GAMESS Input [Write-only]</option>
+                <option value="jin">JIN -- Jaguar input format [Write-only]</option>
+                <option value="mdl">MDL -- MDL MOL format</option>
+                <option value="mmd">MMD -- MacroModel format</option>
+                <option value="mmod">MMOD -- MacroModel format</option>
+                <option value="mol">MOL -- MDL MOL format</option>
+                <option value="mol2">MOL2 -- Sybyl Mol2 format</option>
+                <option value="mopcrt">MOPCRT -- MOPAC Cartesian format</option>
+                <option value="mpd">MPD -- Sybyl descriptor format [Write-only]</option>
+                <option value="mpqcin">MPQCIN -- MPQC simplified input format [Write-only]</option>
+                <option value="nw">NW -- NWChem input format [Write-only]</option>
+                <option value="pcm">PCM -- PCModel Format</option>
+                <option value="pdb">PDB -- Protein Data Bank format</option>
+                <option value="pov">POV -- POV-Ray input format [Write-only]</option>
+                <option value="pqs">PQS -- Parallel Quantum Solutions format</option>
+                <option value="qcin">QCIN -- Q-Chem input format [Write-only]</option>
+                <option value="report">REPORT -- Open Babel report format [Write-only]</option>
+                <option value="rxn">RXN -- MDL RXN format</option>
+                <option value="sd">SD -- MDL MOL format</option>
+                <option value="sdf">SDF -- MDL MOL format</option>
+                <option value="smi">SMI -- SMILES format</option>
+                <option value="tmol">TMOL -- TurboMole Coordinate format</option>
+                <option value="txyz">TXYZ -- Tinker MM2 format [Write-only]</option>
+                <option value="unixyz">UNIXYZ -- UniChem XYZ format</option>
+                <option value="vmol">VMOL -- ViewMol format</option>
+                <option value="xed">XED -- XED format [Write-only]</option>
+                <option value="xyz">XYZ -- XYZ cartesian coordinates format</option>
+                <option value="yob">YOB -- YASARA.org YOB format</option>
+                <option value="zin">ZIN -- ZINDO input format [Write-only]</option>
+              </select>
+"""
 
     def parseFormula(self, raw_formula):
         """Parse formula and return the HTML formula
@@ -57,8 +137,8 @@ class CMLWriter:
         xhtmlout.setOutput(self.fout)
         xhtmlout.setTitle(title)
         xhtmlout.addHead('    <script src="' + '../' * level + 'jmol/Jmol.js" type="text/javascript"></script>')
-        xhtmlout.addHead('    <link rel="stylesheet" type="text/css" href="' + '../' * level + 'styles/style.css" />')
-        xhtmlout.addHead('    <link rel="shortcut icon" href="' + '../' * level + 'images/favicon.ico" />')
+        xhtmlout.addHead('    <link href="' + '../' * level + 'styles/style.css" rel="stylesheet" type="text/css" />')
+        xhtmlout.addBody(self.scriptFormat)
         xhtmlout.addBody('    <div id="header">')
         xhtmlout.addBody('      <img src="'+ '../' * level + 'images/header.png" alt="Header image" />')
         xhtmlout.addBody('    </div>')
@@ -152,6 +232,7 @@ class CMLWriter:
         xhtmlout.addBody('            <li>' + self.l10n.translate('In CML format', lang) + '&nbsp;<a href="' + cml_file + '" title="CML"><img src="' + '../'*level + 'images/download.png" alt="Download cml file" /></a></li>')
         if os.path.isfile(mol_file):
             xhtmlout.addBody('            <li>' + self.l10n.translate('In MOL format', lang) + '&nbsp;<a href="' + mol_file + '" title="MOL"><img src="' + '../'*level + 'images/download.png" alt="Download mol file" /></a></li>')
+        xhtmlout.addBody('            <li>' + self.l10n.translate('Other formats', lang) + '&nbsp;'+self.selection+'</li>')
         xhtmlout.addBody('          </ul>')
         xhtmlout.addBody('        </div>')
         xhtmlout.addBody('        <div id="inchi">')
